from bs4 import BeautifulSoup
import requests
from datetime import date, datetime


class metal_properties:
    def __init__(self, metal, url):
        self.name = metal
        self.content = extract_content(url)
        self.html = analyze_html(self.content)
        self.price = scrape_price(self.html)
        self.date = date.today().strftime("%m.%d.%y")
        self.time = datetime.now().strftime("%H:%M:%S")


def extract_content(url):
    headers = {
        "User-Agent": "Mozilla/5.0 (X11; Linux x86_64; rv:124.0) Gecko/20100101 Firefox/124.0"
    }

    content = requests.get(url, headers=headers)

    if content.status_code == 200:
        return content.text

    return None


def analyze_html(html):
    soup = BeautifulSoup(html, "html.parser")

    return soup


def scrape_price(soup):

    for listItem in soup.find_all("span", id="metal-priceask"):

        listItem = listItem.text.strip('$USD"')

        listItem = listItem.replace(",", "")

        listItem = float(listItem)

        return listItem


if __name__ == "__main__":

    extract_content(url)

from scrape import metal_properties
import pandas
import matplotlib.pyplot as plt
from matplotlib.dates import DateFormatter

currently_tracked_metals = ["gold", "silver", "platinum", "palladium"]

base_url = "https://www.apmex.com/"


def create_url(base_url, metal):
    url = base_url + metal + "-price"

    return url


def get_metal_properties(metal, url):
    metal = metal_properties(metal, url)

    metal = {
        "name": metal.name,
        "price": metal.price,
        "date": metal.date,
        "time": metal.time,
    }

    return metal

# TODO csv test and file creation
# TODO create way to make the data frame into a graph, multi lines on line graph

if __name__ == "__main__":
    all_metals = []

    for metal in currently_tracked_metals:
        url = create_url(base_url, metal)

        metal = get_metal_properties(metal, url)

        all_metals.append(metal)

    csv_data_frame = pandas.DataFrame.from_dict(all_metals)
        
    print(csv_data_frame)

    csv_data_frame.to_csv("metal_prices.csv", mode="a", index=False, header=False)

    graph_data_frame = pandas.read_csv(
        "metal_prices.csv", index_col=0, parse_dates=True, date_format="dateutil"
    )

    print(graph_data_frame)
